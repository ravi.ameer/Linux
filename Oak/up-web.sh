# Shutdown Services
sudo systemctl stop nginx php-fpm paster celery

# Update Joomla
cd '/var/www/main'
sudo -u nginx git pull origin staging
sync

# Update and do maintenance on GNU social
cd '/var/www/social'
sudo -u nginx '/var/www/social/scripts/stopdaemons.sh'
sudo -u nginx git pull origin nightly
sudo -u nginx php '/var/www/social/scripts/upgrade.php'
sudo -u nginx php '/var/www/social/scripts/remove_duplicate_file_urls.php' -y
sudo -u nginx php '/var/www/social/scripts/clean_thumbnails.php' -y
sudo -u nginx php '/var/www/social/scripts/clean_file_table.php' -y
sudo -u nginx php '/var/www/social/scripts/checkschema.php'
sync

# Update WordPress
cd '/var/www/blog'
sudo -u nginx git pull origin master
sync

# Update Twenty Sixteen WordPress Theme
cd '/var/www/blog/wp-content/themes/twentysixteen'
sudo -u nginx git pull origin master
sync

# Update Let's Encrypt
cd '/home/espionage724/letsencrypt'
sudo -u espionage724 git pull origin master
sync

# Update MediaGoblin
cd '/var/www/media'
sudo -u nginx git pull origin master
sudo -u nginx git submodule update
sudo -u nginx '/var/www/media/bin/python' '/var/www/media/setup.py' develop --upgrade
sudo -u nginx '/var/www/media/bin/gmg' dbupdate
sync

# Restart Services
sudo systemctl start nginx php-fpm paster celery
sudo -u nginx '/var/www/social/scripts/startdaemons.sh'

# Finish up
cd '/home/espionage724/'