Computer Role: Game server host

cron.txt
- Cron commands to run scripts
- Resides at /home/espionage724

in.sh
- Stops services
- Updates and compiles Xonotic (git)
- Installs TrinityCore and PvPGN
- Fixes file permissions for PvPGN
- Ran after up.sh
- Resides at /home/espionage724

r.sh
- Reboot and history wipe script
- Resides at /home/espionage724

up.sh
- Updates and recompiles TrinityCore and PvPGN
- Resides at /home/espionage724