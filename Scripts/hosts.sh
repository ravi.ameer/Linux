# Start
cd '/home/espionage724'

# Get updater
wget 'https://github.com/StevenBlack/hosts/archive/master.zip'
unzip '/home/espionage724/master.zip'
rm '/home/espionage724/master.zip'
cd '/home/espionage724/hosts-master'

# Run updater
chmod +x '/home/espionage724/hosts-master/updateHostsFile.py'
python '/home/espionage724/hosts-master/updateHostsFile.py'

# Finish
rm -R '/home/espionage724/hosts-master'
sync
cd '/home/espionage724'