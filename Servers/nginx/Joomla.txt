%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Joomla setup for Realm of Espionage
- Fedora 23 Server

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Software
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo dnf install php-pecl-zip

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Git Management
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cd '/var/www' && sudo git clone -b staging https://github.com/joomla/joomla-cms.git '/var/www/main' && sudo restorecon -v -R '/var/www/main' && sudo chcon -R -t httpd_sys_rw_content_t '/var/www/main' && sudo chown -R nginx:nginx '/var/www/main' && sudo chgrp -R nginx '/var/lib/php/session' '/var/lib/php/wsdlcache' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

CREATE DATABASE joomla;

CREATE USER joomla;
SET PASSWORD FOR 'joomla' = PASSWORD ('x');

GRANT ALL PRIVILEGES ON joomla.* to 'joomla'@'localhost' IDENTIFIED BY 'x';

FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% nginx Server Block
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo nano '/etc/nginx/conf.d/main.conf'

-------------------------
server {
    listen 443 ssl spdy deferred;
    server_name realmofespionage.xyz;
    server_name_in_redirect off;
    root /var/www/main;
    index index.php;

    include /etc/nginx/default.d/php.conf;

    #access_log /var/log/nginx/access.log main;
    #error_log /var/log/nginx/error.log info;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~* /(images|cache|media|logs|tmp)/.*\.(php|pl|py|jsp|asp|sh|cgi)$ {
        return 403;
        error_page 403 /403_error.html;
    }

    location ~* \.(ico|pdf|flv)$ {
        expires 1y;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|swf|xml|txt)$ {
        expires 14d;
    }
}
-------------------------

sudo systemctl restart nginx php-fpm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Setup
%%%%%%%%%%%%%%%%%%%%

- Go to https://realmofespionage.xyz

%%%%%%%%%%%%%%%%%%%%
%%%%% Style
%%%%%%%%%%%%%%%%%%%%

Megamenu:
Fading 300ms

Brand Primary Color:
#3D8BFF

Footer Background:
#484848

Footer Text Color:
#FFFFFF

%%%%%%%%%%%%%%%%%%%%
%%%%% Purity III Footer Text
%%%%%%%%%%%%%%%%%%%%

sudo -u nginx wget https://licensebuttons.net/l/by-sa/4.0/80x15.png -O '/var/www/main/images/cc-by-sa-40.png'

sudo -u nginx nano '/var/www/main/templates/purity_iii/tpls/blocks/footer.php'

-------------------------
          <small>
            <img src="https://realmofespionage.xyz/images/cc-by-sa-40.png" width="80" height="15" /> All content and data available on Realm of Espionage is available under the <a href="https://creativecommons.org/licenses/by-sa/4.0" target="_blank">Creative Commons Attribution-ShareAlike 4.0 International</a> license.
          </small>
-------------------------

%%%%%%%%%%%%%%%%%%%%
%%%%% Purity III Footer Fix
%%%%%%%%%%%%%%%%%%%%

sudo -u nginx nano '/var/www/main/templates/purity_iii/css/custom.css'

-------------------------
/* Footer Styling */

.t3-copyright {
background: #484848 none repeat scroll 0 0;
border-top: 1px solid #eeeeee;
bottom: 0;
font-size: 12px;
left: 0;
padding: 15px 0 0px;
position: fixed;
width: 100%;
}

.t3-footer {
/*padding-bottom: 209px;*/
bottom: 0;
font-size: 12px;
left: 0;
}
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Update Script
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/main-up.sh' && chmod +x '/home/espionage724/main-up.sh'

-------------------------
cd '/var/www/main'
sudo -u nginx git pull origin staging
sync
cd '/home/espionage724'
-------------------------

'/home/espionage724/main-up.sh'

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################