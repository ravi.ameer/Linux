%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- WordPress setup for Realm of Espionage
- Fedora 23 Server

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Git Management
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cd '/var/www' && sudo git clone -b master 'https://github.com/WordPress/WordPress.git' '/var/www/blog' && cd '/var/www/blog/wp-content/themes' && sudo git clone -b master 'https://github.com/WordPress/twentysixteen.git' '/var/www/blog/wp-content/themes/twentysixteen' && sudo restorecon -v -R '/var/www/blog' && sudo chcon -R -t httpd_sys_rw_content_t '/var/www/blog' && sudo chown -R nginx:nginx '/var/www/blog' && sudo chgrp -R nginx '/var/lib/php/session' '/var/lib/php/wsdlcache' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Database
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mysql -u root -p

CREATE DATABASE wordpress;

CREATE USER wordpress;
SET PASSWORD FOR 'wordpress' = PASSWORD ('x');

GRANT ALL PRIVILEGES ON wordpress.* to 'wordpress'@'localhost' IDENTIFIED BY 'x';

FLUSH PRIVILEGES;
EXIT

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% nginx Server Block
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sudo nano '/etc/nginx/conf.d/blog.conf'

-------------------------
server {
    listen 443 ssl spdy;
    server_name blog.realmofespionage.xyz;
    server_name_in_redirect off;
    root /var/www/blog;
    index index.php;

    include /etc/nginx/default.d/php.conf;

    #access_log /var/log/nginx/access.log main;
    #error_log /var/log/nginx/error.log info;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    rewrite /wp-admin$ $scheme://$host$uri/ permanent;

    location ~* ^.+\.(ogg|ogv|svg|svgz|eot|otf|woff|mp4|ttf|rss|atom|jpg|jpeg|gif|png|ico|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav|bmp|rtf)$ {
        access_log off; log_not_found off; expires max;
    }
}
-------------------------

sudo systemctl restart nginx php-fpm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial Setup
%%%%%%%%%%%%%%%%%%%%

- Go to https://blog.realmofespionage.xyz

%%%%%%%%%%%%%%%%%%%%
%%%%% Disable Google Fonts
%%%%%%%%%%%%%%%%%%%%

sudo -u nginx wget 'https://downloads.wordpress.org/plugin/disable-google-fonts.1.2.zip' -O '/var/www/blog/wp-content/plugins/disable-google-fonts.zip' && sudo -u nginx unzip '/var/www/blog/wp-content/plugins/disable-google-fonts.zip' -d '/var/www/blog/wp-content/plugins' && sudo -u nginx mv '/var/www/blog/wp-content/plugins/disable-google-fonts/disable-google-fonts.php' '/var/www/blog/wp-content/plugins/disable-google-fonts.php' && sudo -u nginx rm -R '/var/www/blog/wp-content/plugins/disable-google-fonts' '/var/www/blog/wp-content/plugins/disable-google-fonts.zip' && sync

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Update Script
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nano '/home/espionage724/blog-up.sh' && chmod +x '/home/espionage724/blog-up.sh'

-------------------------
cd '/var/www/blog'
sudo -u nginx git pull origin master
cd '/var/www/blog/wp-content/themes/twentysixteen'
sudo -u nginx git pull origin master
sync
cd '/home/espionage724'
-------------------------

'/home/espionage724/blog-up.sh'

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################