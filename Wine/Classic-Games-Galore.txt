%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Information
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

- Creates isolated 32-bit prefix for Classic Games Galore

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Installation
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Classic Games Galore
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX='/home/beowulfsdr/Wine Prefixes/Classic Games Galore' WINEARCH=win32 winecfg

-------------------------
Drives > Add... > E:
Path: /home/beowulfsdr/Disc Images/Classic Games Galore/
-------------------------

WINEPREFIX='/home/beowulfsdr/Wine Prefixes/Classic Games Galore' wine 'E:/setup.exe'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Configuration
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Disable Crash Error Dialog
%%%%%%%%%%%%%%%%%%%%

WINEPREFIX='/home/beowulfsdr/Wine Prefixes/Classic Games Galore' regedit

-------------------------
HKEY_CURRENT_USER > Software > Wine > WineDbg > DWORD: ShowCrashDialog = 0
-------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%
%%%%% Launcher
%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%%%%% Initial
%%%%%%%%%%%%%%%%%%%%

rm -R '/home/beowulfsdr/.local/share/applications/wine/Programs/Atari' && mkdir '/home/beowulfsdr/.local/share/applications/wine/Programs/Classic Games Galore'

%%%%%%%%%%%%%%%%%%%%
%%%%% Classic Board Games
%%%%%%%%%%%%%%%%%%%%

gedit '/home/beowulfsdr/.local/share/applications/wine/Programs/Classic Games Galore/Classic Board Games.desktop'

-------------------------
[Desktop Entry]
Name=Classic Board Games
Exec=env WINEDEBUG=-all WINEPREFIX='/home/beowulfsdr/Wine Prefixes/Classic Games Galore' wine '/home/beowulfsdr/Wine Prefixes/Classic Games Galore/drive_c/Program Files/Atari/Classic Games Galore/ClassicBoard.exe'
Type=Application
StartupNotify=true
Path=/home/beowulfsdr/Wine Prefixes/Classic Games Galore/drive_c/Program Files/Atari/Classic Games Galore
Icon=9675_ClassicGameLauncher.0
-------------------------

####################################################################################################
####################################################################################################
#####
##### End
#####
####################################################################################################
####################################################################################################